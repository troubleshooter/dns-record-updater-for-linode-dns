# DNS Record Updater for Linode DNS

Either of these scripts will grab both the IPv4 and IPv6 (if any) addresses assigned to any WAN you're behind, and, using Linode's DNS API, will update DNS records with same and log changes/errors using logger. In effect, it's a homemade Dynamic DNS updater. Linode's developing a new API so that's why two versions exist.

To use the script, you need:

1.  A Linode API key (for [version 3 of Linode's DNS API](https://www.linode.com/api/dns)) or Personal Access Token (for [version 4 of Linode's DNS API](https://developers.linode.com/v4/introduction)),
2.  the domain ID, and
3.  the resource (called record in v4) IDs of the DNS records you want to update.

The domain and resource IDs don't change, whether you're using version 3 or version 4.

The scripts use terminal-notifier (installed as a gem, see script coments on how to install) to display screen notifications in Mac OS X. The scripts should port nicely to Linux by removing the terminal-notifier code.

If the WAN you're behind has no IPv6 address, edit the script to set `CHECK_IP6=0` otherwise you'll get a lot of error notifications.
