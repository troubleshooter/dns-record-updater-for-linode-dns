#!/bin/sh

# A shell script to update Linode's DNS with WAN IPs
# Works with Mac Os X 10.9+.
# For use with version 3 of Linode's DNS API
# Requires:
#   * terminal-notifier: sudo gem install terminal-notifier
#   * dig
#	* nc
#   * syslog
#   * Linode (https://linode.com) API Key, domain ID and resource IDs
# Will create needed files .wan_ip4.txt and .wan_ip6.txt in
# home directory on first run
# Sends notifications via terminal-notifier for failed/problem
# events/changes.
# Logs to syslog.
# Run as often as you'd like via cron.
# The only modifications you need to make are the Linode-specific
# ones directly below.
# Script is already set to check for both IPv4 and IPv6. Change if
# necessary. See CHECK_IP4 and CHECK_IP6 below.

# Copyright (C) 2017 Terry Roy
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details. http://www.gnu.org/licenses/.


## LINODE-SPECIFIC SETTINGS - MAKE CHANGES HERE
# Linode DNS API variables
API_KEY=<INSERT>
DOMAIN_ID=<INSERT>
RECORD_ID4=<INSERT>
RECORD_ID6=<INSERT>

# If an error occurs updating the DNS records, the error(s) are returned
# in an array. No errors returns empty array ERRORARRAY:[] so test for empty
# otherwise a problem. Note this is Linode-specific.
NO_ERROR_STR="\"ERRORARRAY\":\[\]"

# Turn checks on/off 1 = on, 0 = off
# This is handy for travelling. Some WANs won't have an IPv6
# address assigned. In that case, turn off IPv6 checks here instead of
# getting error notifications every time the cron job is run.
CHECK_IP4=1
CHECK_IP6=1

## LOGGING
# NOTE: Log levels below notice do not show up in Mac OS X logs by
# default so we use notice level instead of info.

# Log error messages to syslog. The actual error message is appended
# to this so keep the space in.
ERR_MSG="/usr/bin/logger -i -t DNS-Updater -p user.err "

# Log other messages to syslog as "Notice".  The actual message is
# appended to this so keep the space in.
NOTICE_MSG="/usr/bin/logger -i -t DNS-Updater -p user.notice "

# Current date and time for terminal-notifier message
LOGTIME=$(date "+%H:%M:%S")

# Shared options for terminal-notifier
# Message is appended so keep the space in.
# Notifications must be manually dismissed.
# You can change the dismissal time for notifications by using, for example,
#   -timeout 30
# right after terminal-notifier in the line below. This will automatically
# close the notification in 30 seconds.
TN="/usr/local/bin/terminal-notifier -sound default -title \"DNS-Update $LOGTIME:\" -subtitle "

## DATA FILES
# Check for existence and if not present, create.
if [ ! -f "$HOME"/.wan_ip4.txt ] && [ "$CHECK_IP4" -eq 1 ]; then
    /usr/bin/touch "$HOME"/.wan_ip4.txt
    $NOTICE_MSG "No IPv4 file, created $HOME/.wan_ip4.txt"
    $TN "No IPv4 file found" -message "Created $HOME/.wan_ip4.txt"
fi

if [ ! -f "$HOME"/.wan_ip6.txt ] && [ "$CHECK_IP6" -eq 1 ]; then
    /usr/bin/touch "$HOME"/.wan_ip6.txt
    $NOTICE_MSG "No IPv6 file found, created $HOME/.wan_ip6.txt"
    $TN "No IPv6 file found" -message "Created $HOME/.wan_ip6.txt"
fi

## RETRIEVE IPS
# Retrieve WAN IPv4
if [ "$CHECK_IP4" -eq 1 ] ; then
    # Test for connecticvity to nameserver
    # Returns '0' if successful and '1' if unsuccessful.
    /usr/bin/nc -z resolver1.opendns.com 53 >/dev/null 2>&1
    ONLINE4=$?
    if [ "$ONLINE4" -ne 0 ] ; then
        $ERR_MSG "No IPv4 connection"
        $TN "IPv4" -message "No connection"
    else
        WAN_IP4=$(/usr/bin/dig +short -4 myip.opendns.com a @resolver1.opendns.com)
        # Quick pseudo-validation
        VALID4=$(echo "$WAN_IP4" | /usr/bin/tr -dc \'.\' | /usr/bin/wc -c)
        if [ "$VALID4" -eq 3 ]; then
            OLD_WAN_IP4=$(cat "$HOME"/.wan_ip4.txt)
            ### Comparison to previous IP
            if [ "$WAN_IP4" != "$OLD_WAN_IP4" ]; then
                UPDATE4=$(curl -s https://api.linode.com/?api_key="$API_KEY"\&api_action=domain.resource.update\&DomainID="$DOMAIN_ID"\&ResourceID="$RECORD_ID4"\&Target="$WAN_IP4")
                # Check there's no message in the error array
                case "$UPDATE4" in
                    *$NO_ERROR_STR* )
                            echo "$WAN_IP4" > "$HOME"/.wan_ip4.txt
                            $NOTICE_MSG "Updated IPv4 from $OLD_WAN_IP4 to $WAN_IP4. Result:$UPDATE4"
                            $TN "IPv4 changed" -message "Updated DNS record. Result:$UPDATE4"
                        ;;
                    "")
                            $ERR_MSG "ERROR: Problem updating IPv4: Empty String. Old IPv4: $OLD_WAN_IP4  New IPv4: $WAN_IP4"
                            $TN "ERROR" -message "Empty string returned during IPv4 update"
                        ;;
                    * )
                            $ERR_MSG "Problem updating IPv4:  Old IPv4: $OLD_WAN_IP4  New IPv4: $WAN_IP4 Error: $UPDATE4"
                            $TN "ERROR" -message "Error in IPv4 update"
                        ;;
                esac
              else
                $NOTICE_MSG "No IPv4 change"
              fi
        else
            # Notify if problem
            $ERR_MSG "\"ERROR: IPv4 resolver invalid response. Response: $WAN_IP4\""
            $TN "ERROR" -message "IPv4 invalid response: $WAN_IP4"
        fi
    fi
fi

# Retrieve WAN IPv6
if [ "$CHECK_IP6" -eq 1 ] ; then
    # Test for connecticvity to nameserver
    # If successful, commands returns '0' and '1' if unsuccessful.
    /usr/bin/nc -z resolver1.ipv6-sandbox.opendns.com 53 >/dev/null 2>&1
    ONLINE6=$?
    if [ "$ONLINE6" -ne 0 ] ; then
        $ERR_MSG "No IPv6 connection"
        $TN "IPv6" -message "No connection"
    else
        WAN_IP6=$(/usr/bin/dig +short -6 myip.opendns.com aaaa @resolver1.ipv6-sandbox.opendns.com)
        # Quick pseudo-validation
        VALID6=$(echo "$WAN_IP6" | /usr/bin/tr -dc \':\' | /usr/bin/wc -c)
        if [ "$VALID6" -gt 3 ]; then
            OLD_WAN_IP6=$(cat "$HOME"/.wan_ip6.txt)
            # Update DNS record
            if [ "$WAN_IP6" != "$OLD_WAN_IP6" ]; then
                UPDATE6=$(curl -s https://api.linode.com/?api_key="$API_KEY"\&api_action=domain.resource.update\&DomainID="$DOMAIN_ID"\&ResourceID="$RECORD_ID6"\&Target="$WAN_IP6")
                # Check there's no message in error array
                case "$UPDATE6" in
                    *$NO_ERROR_STR* )
                            echo "$WAN_IP6" > "$HOME"/.wan_ip6.txt
                            $NOTICE_MSG "Updated IPv6 from $OLD_WAN_IP6 to $WAN_IP6. Result:$UPDATE6"
                            $TN "IPv6 changed" -message "Updated DNS record.  Result:$UPDATE4 "
                        ;;
                    "")
                            $ERR_MSG "ERROR: Problem updating IPv6: Empty String. Old IPv6: $OLD_WAN_IP6  New IPv6: $WAN_IP6"
                            $TN "ERROR" -message "Empty string returned during IPv6 update"
                        ;;
                    * )
                            $ERR_MSG "Problem updating IPv6 Old IPv6: $OLD_WAN_IP6  New IPv6: $WAN_IP6 $UPDATE6"
                            $TN "ERROR" -message "Error in IPv6 record update"
                        ;;
                esac
            else
                    $NOTICE_MSG "No IPv6 change"
            fi
        else
            # Notify if problem
            $ERR_MSG "IPv6 invalid response. Response: $WAN_IP6"
            $TN "ERROR" -message "IPv6: invalid response: $WAN_IP6"
        fi
    fi
fi